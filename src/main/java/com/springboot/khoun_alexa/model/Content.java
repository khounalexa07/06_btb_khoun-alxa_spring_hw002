package com.springboot.khoun_alexa.model;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Data
public class Content {
    private int id;
    @NotEmpty(message = "Please provide a title.")
    private String title;
    @NotEmpty(message = "Please provide a description for your article.")
    private String description;
    private String urlImg;
    private MultipartFile file;

    public Content(int id, String title, String description, String urlImg) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.urlImg = urlImg;
    }
}
