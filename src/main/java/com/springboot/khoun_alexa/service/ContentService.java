package com.springboot.khoun_alexa.service;

import com.springboot.khoun_alexa.model.Content;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ContentService {

    public List<Content> getAllContent();
    public Content findContentById(int id);
    public void addContentMethod(Content article);
    public void updateContentMethod(int id, Content article);
    public void deleteContentMethod(int id);
}