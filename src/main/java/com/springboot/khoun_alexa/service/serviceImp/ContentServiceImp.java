package com.springboot.khoun_alexa.service.serviceImp;
import com.springboot.khoun_alexa.model.Content;
import com.springboot.khoun_alexa.repository.ContentRepository;
import com.springboot.khoun_alexa.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContentServiceImp implements ContentService {

    @Autowired
    ContentRepository contentRepository;

    @Override
    public List<Content> getAllContent() {
        return contentRepository.getAllContent();
    }

    @Override
    public Content findContentById(int id) {
        return contentRepository.findContentById(id);
    }

    @Override
    public void addContentMethod(Content content) {
        contentRepository.addNewContent(content);
    }

    @Override
    public void updateContentMethod(int id, Content content) {
        System.out.println("in service: " + id + " " + content);
        contentRepository.updateContentById(id, content);
    }

    @Override
    public void deleteContentMethod(int id) {
        contentRepository.deleteContentById(id);
    }
}
