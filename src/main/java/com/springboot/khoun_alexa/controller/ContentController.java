package com.springboot.khoun_alexa.controller;

import com.springboot.khoun_alexa.model.Content;
import com.springboot.khoun_alexa.service.ContentService;
import com.springboot.khoun_alexa.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class ContentController {
    @Autowired
    FileStorageService fileStorageService;

    @Autowired
    ContentService contentService;



    @GetMapping("/form-add")
    public String showFormAdd(Model model){
        model.addAttribute("content", new Content());
        return "form-add";
    }

    @GetMapping("/")
    public String index(Model model){
        model.addAttribute("content", new Content());
        model.addAttribute("contents", contentService.getAllContent());
        return "index";
    }

    @PostMapping("/handle-add")
    public String handleAdd(@ModelAttribute @Valid Content content, BindingResult bindingResult){
        if(content.getFile().isEmpty()){
            content.setUrlImg("http://localhost:8080/images/defaulImage.png");
        }else {
            try {
                String filename = "http://localhost:8080/images/"+ fileStorageService.saveFile(content.getFile());
                content.setUrlImg(filename);
            }catch (Exception e){
                System.out.println( e.getMessage());
            }
        }

        if(bindingResult.hasErrors()){
            return "/form-add";
        }
        contentService.addContentMethod(content);

        return "redirect:/";
    }


    @GetMapping("/update-content/{id}")
    public String updateContent(@PathVariable int id, Model model){
        Content resultContent = contentService.findContentById(id);
        model.addAttribute("content", resultContent);
        model.addAttribute("id", id);
        return "form-update";
    }

    @GetMapping("/delete-content/{id}")
    public String deleteContent(@PathVariable int id){
        contentService.deleteContentMethod(id);
        return "redirect:/";
    }

    @PostMapping("/handle-update/{id}")
    public String handleUpdate(@PathVariable int id, @ModelAttribute @Valid Content content, BindingResult bindingResult){
        Content resultContent = contentService.findContentById(id);
        if(content.getFile().isEmpty()){
            content.setUrlImg(resultContent.getUrlImg());
        }else {
            try {
                String filename = "http://localhost:8080/images/"+ fileStorageService.saveFile(content.getFile());
                content.setUrlImg(filename);
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
        }

        if(bindingResult.hasErrors()){
            return "/form-update";
        }
        contentService.updateContentMethod(id, content);

        return "redirect:/";
    }

    @GetMapping("/view-content/{id}")
    public String viewContent(@PathVariable int id,Model model){
        Content resultContent = contentService.findContentById(id);
        model.addAttribute("content", resultContent);

        return "form-view";
    }

}
